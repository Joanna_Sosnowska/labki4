﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Component1 : AbstractComponent
    {
        RequiredInterface interface1;

        public Component1()
        {
            RegisterRequiredInterface<RequiredInterface>();
        }

        public Component1(RequiredInterface interface1)
        {
            this.interface1 = interface1;
        }

        public void Method11()
        {

            interface1.Method1();
           
        }

        public void Method12()
        {
            interface1.Method2();
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (type.IsInterface)
            {
                interface1 = impl as RequiredInterface; 
            }
        }
    }
}
