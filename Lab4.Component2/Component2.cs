﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Component2 : AbstractComponent, RequiredInterface
    {

       public Component2()
        {
            RegisterProvidedInterface<RequiredInterface>(this);
        }

        public void Method1()
        {
            Console.WriteLine("To jest metoda 1 w komponencie 2");
        }

        public void Method2()
        {
            Console.WriteLine("To jest metoda 2 w komponencie 2");
        }




        public override void InjectInterface(Type type, object impl)
        {
           
        }
    }
}
