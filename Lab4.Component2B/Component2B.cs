﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2B
{
    public class Component2B : AbstractComponent, RequiredInterface
    {
        public Component2B()
        {
            RegisterProvidedInterface<RequiredInterface>(this);
        }

        public void Method1()
        {
            Console.WriteLine("Metoda 1 w komponencie 2B");
        }

        public void Method2()
        {
            Console.WriteLine("Metoda 2 w komponencie 2B");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
