﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Contract;

namespace Lab4.MainB
{
    class Program
    {
        static void Main(string[] args)
        {

            Container kontener = new Container();

            Component2B.Component2B c2B = new Component2B.Component2B();
            Component1.Component1 c1 = new Component1.Component1();

            c2B.RegisterProvidedInterface<RequiredInterface>(c2B);
            c1.RegisterRequiredInterface<RequiredInterface>();

            kontener.RegisterComponents(c1, c2B);

            c1.Method11();
            Console.ReadKey();

        }
    }

}
