﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {

            Container kontener = new Container();

            Component1.Component1 c1 = new Component1.Component1();
            Component2.Component2 c2 = new Component2.Component2();

            c2.RegisterProvidedInterface<RequiredInterface>(c2);
            c1.RegisterRequiredInterface<RequiredInterface>();

            kontener.RegisterComponents(c1, c2);

            c1.Method11();
            Console.ReadKey();


            Console.ReadKey();



        }
    }
}
